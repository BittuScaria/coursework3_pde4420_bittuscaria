THis document covers the how to test and execute the heavybaserobot1 package made as the course work 3 of PDE4420.

Package developed by bittu scaria
Support:mohammed usman(faculty)
refernce :roswiki,ros related books ,websites and articles online


Tools:
Ubuntu 16.04
ROS:kinetic
Solidworks 2017



Objective of the course work:
to design build and program a robot that can knock down cans from shelf in the world provided by course guide.



Execution:
i have designed robot arm using Solidworks and exported the urdf file.Base structure have been developed on xml coding as its just box shape.
all the stl files related with package are saved under folder named meshes.

git clone the package to your catkin workspace or any other workspace,under src folder


$ cd catkin_ws

$ catkin_make

$ source devels/setup .bash

Terminal 1
$ roslaunch heavybaserobot1 cw3.world2.launch

This will launch robot on the course work environment on Gazebo.Urdf/xacro used on this launcg file is mobot7armwithsensor.xacro


Terminal 2
$ roslaunch heavybaserobot1 gripper_arm_control.launch

This will launch the control elemenst for the arm control.It has no relation with base movement.This uses the yaml file under config directory named gripper_arm_control1.yaml.Joint state publisher is what been used for the arm control.

Terminal 3
$ rosrun heavybaserobot1 obstacle_avoidance123.py
THis node will make robot to move untill it detects an object on any of the zobes by using laser scan message.We can see the update of teh environment on the screen too.

Terminal 4
$ rosrun teleop_twist_keyboard teleop_twist_keyboard.py
Once reached at shelf move robot using teleop keyboard to the location of shelf.Keep robot almost close and center with shelfand then run next terminal.

Terminal 5
$ rosrun heavybaserobot1 armaction11.py
This will draag out can  on the top of shelf.Once operation is done CTRL+C to stop the node.Adjust robot 1.5m away ,cenetred with shelf and then execute next rosrun

$ rosrun heavybaserobot1 armaction12.py
this will remove 3rd can from bottom.Once operation is done CTRL+C to stop the node.Adjust robot 1.5m away ,cenetred with shelf and then execute next rosrun

$ rosrun heavybaserobot1 armaction13.py
this will remove 2nd  can from bottom.Once operation is done CTRL+C to stop the node.Adjust robot 1.5m away ,cenetred with shelf and then execute next rosrun

$ rosrun heavybaserobot1 armaction15.py
this will remove 1st  can from bottom.Once operation is done CTRL+C to stop the node.Adjust robot 1.5m away ,cenetred with shelf and then execute next rosrun






