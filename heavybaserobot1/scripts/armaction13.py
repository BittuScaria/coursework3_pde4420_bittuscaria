#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import Float64
from geometry_msgs.msg import Twist
import time
def mover():
    pub1 = rospy.Publisher('/gripper_arm/joint1_position_controller/command', Float64, queue_size=10)
    pub2 = rospy.Publisher('/gripper_arm/joint2_position_controller/command', Float64, queue_size=10)
    pub3 = rospy.Publisher('/gripper_arm/joint3_position_controller/command', Float64, queue_size=10)
    pub4 = rospy.Publisher('/gripper_arm/joint4_position_controller/command', Float64, queue_size=10)
    pub5 = rospy.Publisher('/gripper_arm/joint5_position_controller/command', Float64, queue_size=10)
    pub6 = rospy.Publisher('/gripper_arm/joint6_position_controller/command', Float64, queue_size=10)
    pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)
    rospy.init_node('armaction11', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():



	joint1 = Float64
        joint1 = -0.0
        rospy.loginfo(joint1)
        pub1.publish(joint1)
	time.sleep(2)
	joint2 = Float64
        joint2 = -0.0
        rospy.loginfo(joint2)
        pub2.publish(joint2)
	time.sleep(2)
	joint3 = Float64
        joint3 = 0.0
        rospy.loginfo(joint3)
        pub3.publish(joint3)
	time.sleep(2)
	joint4 = Float64
        joint4 = 0.0
        rospy.loginfo(joint1)
        pub4.publish(joint1)
	time.sleep(2)
	joint5 = Float64
        joint5 = -0.0
        rospy.loginfo(joint5)
        pub5.publish(joint5)
	time.sleep(2)
	joint6 = Float64
        joint6 = 0.0
        rospy.loginfo(joint6)
        pub6.publish(joint6)
	time.sleep(2)




	
	joint2 = Float64
        joint2 = 1.4
        rospy.loginfo(joint2)
        pub2.publish(joint2)
	time.sleep(2)
	joint3 = Float64
        joint3 = 0.25
        rospy.loginfo(joint3)
        pub3.publish(joint3)
	time.sleep(2)

	msg = Twist()
        linear_x = 0.55
        angular_z = -1.28
        msg.linear.x = linear_x
        msg.angular.z = angular_z
        pub.publish(msg)
	time.sleep(3)
        linear_x = 0.0
        angular_z = 0.0
        msg.linear.x = linear_x
        msg.angular.z = angular_z
        pub.publish(msg)
	time.sleep(3)

	joint5 = Float64
        joint5 = -0.06
        rospy.loginfo(joint5)
        pub5.publish(joint5)
	time.sleep(2)
	joint6 = Float64
        joint6 = 0.06
        rospy.loginfo(joint6)
        pub6.publish(joint6)
	time.sleep(2)
	joint1 = Float64
        joint1 = -2.0
        rospy.loginfo(joint1)
        pub1.publish(joint1)
	time.sleep(2)
        rate.sleep()

if __name__ == '__main__':
    try:
        mover()
    except rospy.ROSInterruptException:
        pass

