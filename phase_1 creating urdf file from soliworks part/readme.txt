Test project created by Bittu Scaria,Middlesex University Dubai.
It is the part of Course work 3 of PDE4420


this test project aims in converting a solidworks part file to a URDF file and then openeing in RVIZ to view it.

base_link is the solid block of 500mm x 250mm created in Solidworks as part file and then using File-Export to URDF option base_link directory is saved as the result of export step.

Make sure this directory is located in the SRC folder under the workspace so that URDF file can locate the STL mesh files.

Execution steps(follow using the screen shots on the screen shot folder)

1)Open a terminal

$ roslaunch ros_test1 rostest1_rviz.launch model:=base_link.urdf

by launching above you can see RVIZ launches with out the model on it.

2)Add "robot model" on RVIZ

3)Change the Fixed frame "map" to base_link,by mnually entering it.

4)Add TF.


