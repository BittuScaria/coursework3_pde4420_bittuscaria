This package is made by Bittu Scaria of Middlesex university for the course work 3 of PDE4420
THis package completely belongs to Mr.Bittu Scaria and his research effort ,not to copy or duplicate without written confirmation from him through robotbits007@gmail.com

The aim of this package is testing a 3D Cad model buld in Solidworks and then exporting the assemble of a two wheel robot to URDF using the URDF export plugin and then configuring the same in ROS to make it a functional robot in ROS.

wheelbasesolidworks_robot1 package is completely made using URDF export tool on solidworks.
Clone this packge on to the src directory of catkin workspace.
cd to catkin workspace
catkin_make
source devel/setup .bash


$ roslaunch wheelbasesolidworks_robot1 wheelbasesolidworks_robot.launch

executing above command will launch the two wheeler robot on the empty world.

open another Terminal 2

rostopic list----to view all the active topics,,even you can watch for the /cmd_vel


open  Terminal 3
$ rostopic pub /cmd_vel geometry_msgs/Twist "linear: 
  x: -0.5
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.0" -r10
this will make robot to move 

CTRL + C to stop publishing 

$ rostopic pub /cmd_vel geometry_msgs/Twist "linear: 
  x: 0.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.0" 
to stop the robot moving by publishing zero value.
